package com.company.w7hw;

import java.sql.*;

public class Senior extends Company {
    String URL = "jdbc:postgresql://localhost:5432/assignment5";
    String username = "postgres";
    String pass = "ablayhan2003";
    @Override
    public void EmployeeName (){            //i override this method for class Senior which extends from Company class
        try {
            Connection connection = DriverManager.getConnection(URL, username, pass);
            String sql = "SELECT senior FROM company";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);
            int i=1;
            System.out.println("Senior programmer names:");
            while (result.next()){
                String senior = result.getString("senior");
                System.out.println(i+") "+senior);
                i++;
            }
            connection.close();
        } catch(Exception e){
            System.out.println(e);
        }
    }
    public void FutureSeniors() throws ClassNotFoundException, SQLException{   // this is for Juniors class which extends from Senior(this class)
        System.out.println("unknown");
    }
}
