package com.company.w7hw;

import java.sql.*;

public class Junior extends Senior{
    String URL = "jdbc:postgresql://localhost:5432/assignment5";
    String username = "postgres";
    String pass = "ablayhan2003";
    @Override
    public void EmployeeName (){        //i override this method for class Junior which extends from Senior class
        try {
            Connection connection = DriverManager.getConnection(URL, username, pass);
            String sql = "SELECT junior FROM company";
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);
            int j=1;
            System.out.println("Junior programmer names:");
            while (result.next()){
                String junior = result.getString("junior");
                System.out.println(j+") "+junior);
                j++;
            }
            connection.close();
        } catch(Exception e){
            System.out.println(e);
        }
    }
    @Override
    public void FutureSeniors()throws ClassNotFoundException, SQLException{
        try {
            Connection connection = DriverManager.getConnection(URL, username, pass);
            String sql = "SELECT junior FROM company where id=1 or id=3";          //take just random juniors
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);
            int j=1;
            System.out.println("Future Senior programmer names:");
            while (result.next()){
                String junior = result.getString("junior");
                System.out.println(j+") "+junior);
                j++;
            }
            connection.close();
        } catch(Exception e){
            System.out.println(e);
        }
    }
}
